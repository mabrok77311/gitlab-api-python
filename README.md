# GitLab API with Python

Example scripts and code for using the GitLab API with the Python library [python-gitlab](https://python-gitlab.readthedocs.io/en/stable/), explained in the blog post [Efficient DevSecOps workflows: Hands-on python-gitlab API automation](https://about.gitlab.com/blog/2023/02/01/efficient-devsecops-workflows-hands-on-python-gitlab-api-automation/).

Project avatar credits: Photo by <a href="https://unsplash.com/@davidclode?utm_source=unsplash&utm_medium=referral&utm_content=creditCopyText">David Clode</a> on <a href="https://unsplash.com/photos/o8C6UFpqC4s?utm_source=unsplash&utm_medium=referral&utm_content=creditCopyText">Unsplash</a>

## Requirements

- Python >= 3.10
- python-gitlab >= 3.9.0

## Installation

### Source

Install Python and the pip package manager. Install the required package dependencies passing the  [requirements.txt](requirements.txt) file. Example on macOS with Homebrew:

```shell
brew install python

pip3 install -r requirements.txt
```

### Usage

```shell
python3 <scriptname>
```

Environment variables can be exported into the environment:

```shell
export GL_SERVER=https://gitlab.domain.com
```

or passed into the command line call once:

```shell
GL_SERVER=https://gitlab.domain.com python3 <scriptname>
```

Some scripts can be integrated into CI/CD, this is documented separately.

# Use cases

## Get Merge Request Approval Settings

Get all projects from the instance, from a group, or a single project, and pretty print the Merge Request Approval Rules settings in Markdown format.

- Script: [get_mr_approval_rules.py](get_mr_approval_rules.py)
- [Library docs](https://python-gitlab.readthedocs.io/en/stable/gl_objects/merge_request_approvals.html)

Specify the following environment variables to control the behaviour:

- GL_SERVER (default: https://gitlab.com)
- GL_GROUP_ID (to print settings from all projects in the group and sub groups)
- GL_PROJECT_ID (to print settings from a single project)

If no group or project id are specified, all projects from the instance will be printed. Note that this can take a while. Recommended way are using group IDs.

Example:

```shell
GL_GROUP_ID=62378643 GL_TOKEN=$GITLAB_TOKEN GL_SERVER="https://gitlab.com" python3 get_mr_approval_rules.py

GL_GROUP_ID=62378643 GL_TOKEN=$GITLAB_TOKEN GL_SERVER="https://gitlab.com" python3 get_mr_approval_rules.py  > mr_ar.md
```

<details><summary>Click to expand</summary>

# Summary of projects and their merge request approval rules
# Project: Developer Evangelism at GitLab  / use-cases / GitLab API / GitLab API Playground, ID: 42489449


[MR Approval settings](https://gitlab.com/gitlab-de/use-cases/gitlab-api/gitlab-api-playground/-/settings/merge_requests)


## Approval rule: Maintainer approval required , ID: 6684764

```json

{
  "project_id": "42489449",
  "id": 6684764,
  "name": "Maintainer approval required ",
  "rule_type": "regular",
  "eligible_approvers": [
    {
      "id": 5534214,
      "username": "dnsmichi",
      "name": "Michael Friedrich",
      "state": "active",
      "avatar_url": "https://secure.gravatar.com/avatar/383a41b48a683b47432b15e8e86fd285?s=80&d=identicon",
      "web_url": "https://gitlab.com/dnsmichi"
    }
  ],
  "approvals_required": 1,
  "users": [
    {
      "id": 5534214,
      "username": "dnsmichi",
      "name": "Michael Friedrich",
      "state": "active",
      "avatar_url": "https://secure.gravatar.com/avatar/383a41b48a683b47432b15e8e86fd285?s=80&d=identicon",
      "web_url": "https://gitlab.com/dnsmichi"
    }
  ],
  "groups": [],
  "contains_hidden_groups": false,
  "protected_branches": [],
  "applies_to_all_protected_branches": false
}

```

## Approval rule: Protected branch approval , ID: 6684767

```json

{
  "project_id": "42489449",
  "id": 6684767,
  "name": "Protected branch approval ",
  "rule_type": "regular",
  "eligible_approvers": [
    {
      "id": 5534214,
      "username": "dnsmichi",
      "name": "Michael Friedrich",
      "state": "active",
      "avatar_url": "https://secure.gravatar.com/avatar/383a41b48a683b47432b15e8e86fd285?s=80&d=identicon",
      "web_url": "https://gitlab.com/dnsmichi"
    }
  ],
  "approvals_required": 1,
  "users": [
    {
      "id": 5534214,
      "username": "dnsmichi",
      "name": "Michael Friedrich",
      "state": "active",
      "avatar_url": "https://secure.gravatar.com/avatar/383a41b48a683b47432b15e8e86fd285?s=80&d=identicon",
      "web_url": "https://gitlab.com/dnsmichi"
    }
  ],
  "groups": [],
  "contains_hidden_groups": false,
  "protected_branches": [
    {
      "id": 62297497,
      "name": "main",
      "push_access_levels": [
        {
          "id": 68405664,
          "access_level": 40,
          "access_level_description": "Maintainers",
          "user_id": null,
          "group_id": null
        }
      ],
      "merge_access_levels": [
        {
          "id": 67163394,
          "access_level": 40,
          "access_level_description": "Maintainers",
          "user_id": null,
          "group_id": null
        }
      ],
      "allow_force_push": false,
      "unprotect_access_levels": [],
      "code_owner_approval_required": false
    }
  ],
  "applies_to_all_protected_branches": true
}

```

## Approval rule: Coverage-Check, ID: 6684770

```json

{
  "project_id": "42489449",
  "id": 6684770,
  "name": "Coverage-Check",
  "rule_type": "report_approver",
  "eligible_approvers": [
    {
      "id": 5534214,
      "username": "dnsmichi",
      "name": "Michael Friedrich",
      "state": "active",
      "avatar_url": "https://secure.gravatar.com/avatar/383a41b48a683b47432b15e8e86fd285?s=80&d=identicon",
      "web_url": "https://gitlab.com/dnsmichi"
    },
    {
      "id": 260236,
      "username": "abuango",
      "name": "Abubakar Siddiq Ango",
      "state": "active",
      "avatar_url": "https://gitlab.com/uploads/-/system/user/avatar/260236/avatar.png",
      "web_url": "https://gitlab.com/abuango"
    }
  ],
  "approvals_required": 0,
  "users": [
    {
      "id": 260236,
      "username": "abuango",
      "name": "Abubakar Siddiq Ango",
      "state": "active",
      "avatar_url": "https://gitlab.com/uploads/-/system/user/avatar/260236/avatar.png",
      "web_url": "https://gitlab.com/abuango"
    },
    {
      "id": 5534214,
      "username": "dnsmichi",
      "name": "Michael Friedrich",
      "state": "active",
      "avatar_url": "https://secure.gravatar.com/avatar/383a41b48a683b47432b15e8e86fd285?s=80&d=identicon",
      "web_url": "https://gitlab.com/dnsmichi"
    }
  ],
  "groups": [],
  "contains_hidden_groups": false,
  "protected_branches": [],
  "applies_to_all_protected_branches": false
}

```


</details>

## Automated commenter

Update a given list of issues/epics with DRI tags and generic reminder message.

External project: [GitLab Automated Commenter](https://gitlab.com/gitlab-de/gitlab-api-automated-commenter)