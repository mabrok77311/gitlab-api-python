#!/usr/bin/env python

# Description: Showcases how to use the GitLab object with no additional configuration (unauthenticated requests)
# Requirements: python-gitlab Python libraries. GitLab API read access, and maintainer access to all configured groups/projects.
# Author: Michael Friedrich <mfriedrich@gitlab.com>
# License: MIT, (c) 2023-present GitLab B.V.

import gitlab

gl = gitlab.Gitlab()

# Get .gitignore templates without authentication
# https://python-gitlab.readthedocs.io/en/stable/gl_objects/templates.html#gitignore-templates
gitignore_templates = gl.gitignores.get('Python')

print(gitignore_templates.content)
