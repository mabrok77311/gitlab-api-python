#!/usr/bin/env python

# Description: Get the license from a project using multiple techniques. Can be used on a group ID too.
# Requirements: python-gitlab Python libraries. GitLab API read access, and developer access to all configured groups/projects.
# Author: Michael Friedrich <mfriedrich@gitlab.com>
# License: MIT, (c) 2023-present GitLab B.V.

import gitlab
import os
import sys

GITLAB_SERVER = os.environ.get('GL_SERVER', 'https://gitlab.com')
GITLAB_TOKEN = os.environ.get('GL_TOKEN')
PROJECT_ID = os.environ.get('GL_PROJECT_ID') # Test data: 42491852 https://gitlab.com/gitlab-de/use-cases/gitlab-api/gitlab-api-python
GROUP_ID = os.environ.get('GL_GROUP_ID') # Test data: 16058698 https://gitlab.com/gitlab-de/use-cases

if not GITLAB_TOKEN:
    print("Please set the GL_TOKEN env variable.")
    sys.exit(1)

gl = gitlab.Gitlab(GITLAB_SERVER, private_token=GITLAB_TOKEN)

LICENSE_FILE_PATTERNS = [
    "license",
    "license.md",
    "licence",
    "license.txt"
]

# TODO
LICENSE_VALID_OSS = [

]

# Collect all projects, or prefer projects from a group id, or a project id
projects = []

# Direct project ID
if PROJECT_ID:
    projects.append(gl.projects.get(PROJECT_ID, license=True, iterator=True, pagination="keyset", order_by="updated_at", per_page=100))

# Groups and projects inside
elif GROUP_ID:
    group = gl.groups.get(GROUP_ID)

    for project in group.projects.list(include_subgroups=True, all=True):
        # https://python-gitlab.readthedocs.io/en/stable/gl_objects/groups.html#examples
        manageable_project = gl.projects.get(project.id, license=True, iterator=True, pagination="keyset", order_by="updated_at", per_page=100)
        projects.append(manageable_project)

# All projects on the instance (may take a while to process)
else:
    projects = gl.projects.list(get_all=True, license=True, iterator=True, pagination="keyset", order_by="updated_at", per_page=100)


# Goal: Try to download License from all projects
# We don't know the filename yet, so let's include license data following https://docs.gitlab.com/ee/api/projects.html#get-single-project

project_found = {}

for project in projects:

    # debug
    #print(project.license_url)
    #print(project.license)

    if project.license_url:
        print("Found project {name} license in {url}".format(name=project.path_with_namespace, url=project.license_url))
        license = project.license
        # print(license) #debug
        print("License: {name} ({html_url})".format(name=license['name'], html_url=license['html_url']))

        # TODO: Check whether this license is allowed for OSS program application.

        if project.path_with_namespace not in project_found:
            project_found[project.path_with_namespace] = True


    # If there was not project setting for the license, try to get the license file from a few matched patterns in the repository tree.

    # Search the file in the repository tree and get the raw blob
    for f in project.repository_tree(iterator=True):
        # print("File path '{name}' with id '{id}'".format(name=f['name'], id=f['id']))

        # Skip projects that already contained a valid project setting
        if project.path_with_namespace in project_found:
            continue

        if str(f['name']).lower() in LICENSE_FILE_PATTERNS:
            f_content = project.repository_raw_blob(f['id'])
            print("License content: {c}".format(c=f_content))

            # TODO: Check whether this license is allowed for OSS program application.






