#!/usr/bin/env python

import gitlab
import os
from datetime import date

SERVER='https://gitlab.com'
PROJECT_ID=17255375 # https://gitlab.com/dnsmichi/api-playground
FILE_NAME='index.md'
REF_NAME='main'

gl = gitlab.Gitlab(SERVER, private_token=os.environ['GITLAB_TOKEN'])

# https://python-gitlab.readthedocs.io/en/stable/gl_objects/projects.html
p = gl.projects.get(PROJECT_ID)

labels = p.labels.list()

index={}

for i in p.issues.list():
    for l in i.labels:
        if l not in index:
            index[l] = []

        index[l].append("#{id} - {title}".format(id=i.id, title=i.title))



index_str = """# Issue Overview
_Grouped by issue labels._
"""

for l_name, i_list in index.items():
    index_str += "\n## {label} \n\n".format(label=l_name)

    for i in i_list:
        index_str += "- {title}\n".format(title=i)

# DEBUG
print(index_str)

# Dump index_str to FILE_NAME
# Create as new commit
# See https://docs.gitlab.com/ce/api/commits.html#create-a-commit-with-multiple-files-and-actions
# for actions detail

# Check if file exists, and define commit action
f = p.files.get(file_path=FILE_NAME, ref=REF_NAME)
if not f:
    action='create'
else:
    action='update'


data = {
    'branch': REF_NAME,
    'commit_message': 'Generate new index, {d}'.format(d=date.today()),
    'actions': [
        {
            'action': action,
            'file_path': FILE_NAME,
            'content': index_str
        }
    ]
}

commit = p.commits.create(data)
