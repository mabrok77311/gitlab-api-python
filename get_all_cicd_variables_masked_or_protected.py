#!/usr/bin/env python

# Description: Get the list of instance, groups and projects CI/CD variables and check whether variables are protected or masked as potential secrets.
# Requirements: python-gitlab Python libraries. GitLab API read access, and maintainer +access to all configured groups/projects (and instance admin if available)
# Author: Michael Friedrich <mfriedrich@gitlab.com>
# License: MIT, (c) 2023-present GitLab B.V.

import gitlab
import os
import sys

# Helper function to evaluate secrets and print the variables
def eval_print_var(var):
    if var.protected or var.masked:
        print("🛡️🛡️🛡️ Potential secret: Variable '{name}', protected {p}, masked: {m}".format(name=var.key,p=var.protected,m=var.masked))

GITLAB_SERVER = os.environ.get('GL_SERVER', 'https://gitlab.com')
GITLAB_TOKEN = os.environ.get('GL_TOKEN') # token requires maintainer+ permissions. Instance variables require admin access.
PROJECT_ID = os.environ.get('GL_PROJECT_ID') #optional
GROUP_ID = os.environ.get('GL_GROUP_ID', 8034603) # https://gitlab.com/everyonecancontribute

if not GITLAB_TOKEN:
    print("🤔 Please set the GL_TOKEN env variable.")
    sys.exit(1)

gl = gitlab.Gitlab(GITLAB_SERVER, private_token=GITLAB_TOKEN)

# Collect all projects, or prefer projects from a group id, or a project id
projects = []
# Collect all groups, or prefer group from a group id
groups = []

# Direct project ID
if PROJECT_ID:
    projects.append(gl.projects.get(PROJECT_ID))

# Groups and projects inside
elif GROUP_ID:
    group = gl.groups.get(GROUP_ID)

    for project in group.projects.list(include_subgroups=True, all=True):
        # https://python-gitlab.readthedocs.io/en/stable/gl_objects/groups.html#examples
        manageable_project = gl.projects.get(project.id)
        projects.append(manageable_project)

    groups.append(group)

# All projects/groups on the instance (may take a while to process, use iterators to fetch on-demand).
else:
    projects = gl.projects.list(iterator=True)
    groups = gl.groups.list(iterator=True)

print("# List of all CI/CD variables marked as secret (instance, groups, projects)")

# https://python-gitlab.readthedocs.io/en/stable/gl_objects/variables.html

# Instance variables (if the token has permissions)
print("Instance variables, if accessible")
try:
    for i_var in gl.variables.list(iterator=True):
        eval_print_var(i_var)
except:
    print("No permission to fetch global instance variables, continueing without.")
    print("\n")

# group variables (maintainer permissions for groups required)
for group in groups:
    print("Group {n}, URL: {u}".format(n=group.full_path, u=group.web_url))
    for g_var in group.variables.list(iterator=True):
        eval_print_var(g_var)

    print("\n")

# Loop over projects and print the settings
for project in projects:
    # skip archived projects, they throw 403 errors
    if project.archived:
        continue

    print("Project {n}, URL: {u}".format(n=project.path_with_namespace, u=project.web_url))
    for p_var in project.variables.list(iterator=True):
        eval_print_var(p_var)

    print("\n")



